<?hh //strict
/**
 * 
 * @author Timothy Chandler <tim@geomash.com>
 * @package nutsNBolts
 */
namespace demoApp
{
	use nutsNBolts\plugin\application\ApplicationInstaller;
	use nutsNBolts\model\Registry;
	use nutsNBolts\model\Route;
	use nutsNBolts\model\Container;
	use nutsNBolts\model\Signal;
	use nutsNBolts\plugin\navigation\model\Nav;
	use nutsNBolts\plugin\service\model\Service;
	use nutsNBolts\plugin\service\model\ServiceRoute;
	use nutshell\plugin\user\User;
	use nutshell\plugin\user\model\User as UserModel;
	
	class Installer extends ApplicationInstaller
	{
		private Map<string,mixed> $routes		=Map{};
		private Map<string,mixed> $serviceRoutes=Map{};
		
		public function install():bool
		{
			if (parent::install())
			{
				$this->installRoutes();
				$this->installContainers();
				$this->installNavItems();
				$this->installSignals();
				return true;
			}
			return false;
		}
		
		public function uninstall():bool
		{
			$applicationRegRef=$this->getRegistryRef();
			
			Nav::delete(Map{'registeredBy'=>$applicationRegRef});
			Route::delete(Map{'registeredBy'=>$applicationRegRef});
			Service::delete(Map{'registeredBy'=>$applicationRegRef});
			ServiceRoute::delete(Map{'registeredBy'=>$applicationRegRef});
			Container::delete(Map{'registeredBy'=>$applicationRegRef});
			Signal::delete(Map{'registeredBy'=>$applicationRegRef});
			return parent::uninstall();
		}
		  
		private function installRoutes():bool
		{
			$applicationRegRef=$this->getRegistryRef();
			$adminService=Service::create();
			$adminService->setRegisteredBy($applicationRegRef);
			$adminService->setName('New app service');
			$adminService->setRef('APP');
			$adminService->setURIBinding('/app');
			$adminService->setStatus(1);
			$adminService->save();
			
			$this->routes['index']=Route::create();
			$this->routes['index']->setRegisteredBy($applicationRegRef);
			$this->routes['index']->setPath('/something');
			$this->routes['index']->setAction('dmeoApp\\controller\\Index@_index');
			$this->routes['index']->setMethod('GET');
			$this->routes['index']->save();
			
			$this->serviceRoutes['index']=ServiceRoute::create();
			$this->serviceRoutes['index']->setRegisteredBy($applicationRegRef);
			$this->serviceRoutes['index']->setService($adminService);
			$this->serviceRoutes['index']->setRoute($this->routes['index']);
			$this->serviceRoutes['index']->save();
			return true;
		}
		
		private function installContainers():void
		{

		}
		
		private function installNavItems():void
		{
		}
		
		public function installSignals():void
		{
		}
	}
}