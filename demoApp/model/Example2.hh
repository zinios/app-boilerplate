<?hh //strict
namespace demoApp\model
{
	use nutshell\plugin\database\orm\Model;
	
	/**
	 * @Collection example2
	 */
	class Example2 extends Model
	{
		/**
		 * @Id
		 */
		public mixed $id=null;
		
		/**
		 * @String
		 */
		public ?string $phoneNumber=null;
		
	}
}