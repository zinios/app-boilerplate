<?hh //strict

namespace demoApp\controller
{
	use nutshell\plugin\application\controller\Controller;
	use nutshell\plugin\application\controller\Viewable;
	
	class IndexView extends Controller
	{
		use Viewable;
		
		public function run():void
		{
			return $this->render('demoApp/template/index.twig.tpl');
		}


		public function getData(Map $data):void
		{
			
			$this->set('data', $data);
			return $this->render('demoApp/template/showData.twig.tpl');
		}
		
	}
}