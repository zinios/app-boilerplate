<?hh //strict

namespace demoApp\controller
{
	use nutshell\plugin\application\controller\Controller;
	use nutshell\plugin\application\controller\Passable;
	use nutshell\plugin\application\router\RouterException;
	use app\model\Example;
 	use app\model\Example2;

	class Index extends Controller
	{
		use Passable;
		
		public function _index():void
		{
			//show hello world 
			print $this->passControlTo('demoApp\\controller\\IndexView');
		}

		public function showData():void
		{
			print $this->passControlTo('demoApp\\controller\\IndexView','getData', $data);

		}
	}
}
