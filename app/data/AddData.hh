<?hh //strict

namespace app\data
{
	use nutshell\plugin\application\controller\Controller;
	use nutshell\plugin\application\controller\Restful;
	use nutshell\plugin\application\router\RouterException;
 	use app\model\Example;
 	use app\model\Example2;

	class AddData extends Controller
	{
		use Restful;
		
		public function __index():void
		{
			$example2 = Example2::create();
			$example2->setPhoneNumber('1234566788');
			$example2->save();

			$example = Example::create();
			$example->setName('Foo');
			$example->setExample2($example2);
			$example->save();

			die('data has been added to the database');
		}
	}
}