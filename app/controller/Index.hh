<?hh //strict

namespace app\controller
{
	use nutshell\plugin\application\controller\Controller;
	use nutshell\plugin\application\controller\Passable;
	use nutshell\plugin\application\router\RouterException;
	use app\model\Example;
 	use app\model\Example2;

	class Index extends Controller
	{
		use Passable;
		
		public function _index():void
		{
			//show hello world 
			print $this->passControlTo('app\\controller\\IndexView');
		}

		public function showData():void
		{
			print $this->passControlTo('app\\controller\\IndexView','getData', $data);

		}
	}
}
