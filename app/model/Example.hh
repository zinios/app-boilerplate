<?hh //strict
namespace app\model
{
	use nutshell\plugin\database\orm\Model;
	
	/**
	 * @Collection example
	 */
	class Example extends Model
	{
		/**
		 * @Id
		 */
		public mixed $id=null;
		
		/**
		 * @String
		 */
		public ?string $name=null;
		
				
		/**
		 * @Relate app\model\Example2
		 */
		public ?string $example2=null;
	
	}
}