<?hh //partial
namespace
{
	const string PUBLIC_DIR=__DIR__.DIRECTORY_SEPARATOR;
	
	//Include the composer autoloader to kick things off.
	require_once('../vendor/autoload.php');
	
	//GO GO GO!!!
	new cookieCutter\CookieCutter('/',__DIR__.'/../app/config/');
	new app\App('/',__DIR__.'/../app/config/');
	$NNB=nutsNBolts\NutsNBolts::getInstance('/',__DIR__.'/../app/config/');
	$NNB->exec();
}